provider "kubernetes" {
  host                   = aws_eks_cluster.demo.endpoint
  cluster_ca_certificate = base64decode(aws_eks_cluster.demo.certificate_authority[0].data)
  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = ["eks", "get-token", "--cluster-name", var.cluster-name]
    command     = "aws"
  }
}

provider "kubectl" {
  host                   = aws_eks_cluster.demo.endpoint
  cluster_ca_certificate = base64decode(aws_eks_cluster.demo.certificate_authority[0].data)
  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = ["eks", "get-token", "--cluster-name", var.cluster-name]
    command     = "aws"
  }
}

resource "kubernetes_config_map" "dag-requirements" {
  metadata {
    name = "requirements"
  }

  data = {
    "requirements.txt" = file("${path.module}/../values/requirements.txt")
  }
}

data "kubectl_file_documents" "airflow-data" {
  content = file("${path.module}/../kubernetes-files/secrets.yaml")
}

resource "kubectl_manifest" "airflow-secrets" {
    count     = length(data.kubectl_file_documents.airflow-data.documents)
    yaml_body = element(data.kubectl_file_documents.airflow-data.documents, count.index)
}

# resource "kubernetes_secret" "airflow" {
#   metadata {
#     name = "airflow"
#   }

#   data = {
#     airflow-fernetKey = "eGZ4S1FqWVNmU2xHbGhpTWpGWGRMSHJmSXZhUG92Sk0="
#     airflow-password = "airflow"
#   }

#   type = "opaque"
# }

# resource "kubernetes_secret" "postgres" {
#   metadata {
#     name = "postgres"
#   }

#   data = {
#     postgresql-password = "gYQs0SB62s"
#     postgresql-postgres-password = "0nJsOlaq3p"
#   }

#   type = "opaque"
# }

# resource "kubernetes_secret" "redis-password" {
#   metadata {
#     name = "redis-password"
#   }

#   data = {
#     redis-password = "ZLCva6CQe3"
#   }

#   type = "opaque"
# }